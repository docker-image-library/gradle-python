FROM gradle:4.8.1-alpine

USER root
 RUN echo http://mirror.yandex.ru/mirrors/alpine/v3.5/main > /etc/apk/repositories && \
     echo http://mirror.yandex.ru/mirrors/alpine/v3.5/community >> /etc/apk/repositories && \
     apk update && \
     apk add --no-cache python3 postgresql-client curl && \
     python3 -m ensurepip && \
     rm -r /usr/lib/python*/ensurepip && \
     pip3 install --upgrade pip setuptools && \
     if [ ! -e /usr/bin/pip ]; then ln -s pip3 /usr/bin/pip ; fi && \
     if [[ ! -e /usr/bin/python ]]; then ln -sf /usr/bin/python3 /usr/bin/python; fi && \
     pip install tavern colorama && \
     rm -r /root/.cache
USER gradle